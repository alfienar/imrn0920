// No.1 Range
function range(startNum, finishNum) {
    var array = []
    if(startNum < finishNum) {
        for(startNum; startNum <= finishNum; startNum++) {
            array.push(startNum)
        } return array
    } else if(startNum > finishNum) {
        for(startNum; startNum >= finishNum; startNum--) {
            array.push(startNum)
        } return array
    } else {
        return -1
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1


// No.2 Range with step
function rangeWithStep(startNum, finishNum, step) {
    var array = []
    if(startNum < finishNum) {
        for(startNum; startNum <= finishNum; startNum += step) {
            array.push(startNum)
        } return array
    } else if(startNum > finishNum) {
        for(startNum; startNum >= finishNum; startNum -= step) {
            array.push(startNum)
        } return array
    } else {
        return -1
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]


// No.3 Sum of range
function sum(startNum, finishNum=0, step=1) {
    var angka = rangeWithStep(startNum, finishNum, step)
    var jarak = angka.length
    var jumlah = 0
    for(i = 0; i < jarak; i++) {
        jumlah += angka[i]
    } return jumlah
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

// No.4 Multidimensional array
function dataHandling(input) {
    var n = input.length
    var biodata = []
    var item = ["Nomor ID: ", "Nama Lengkap: ", "TTL: ", "Hobi: "]
    for(i = 0; i < n; i++) {
        for(j = 0; j < n; j++) {
            switch(j) {
                case 0:
                    biodata += item[0] + input[i][0] + "\n"
                    break
                case 1:
                    biodata += item[1] + input[i][1] + "\n"
                    break
                case 2:
                    biodata += item[2] + input[i][2] + " " + input[i][3] + "\n"
                    break                
                case 3:
                    biodata += item[3] + input[i][4] + "\n" + "\n"
                    break
                case 4:
                    break
            }
        }
    } return biodata
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

console.log(dataHandling(input))

// No.5 Balik kata
function balikKata(kata) {
    var n = kata.length - 1
    var balik = ""
    for(n; n >= 0; n--) {
        balik += kata[n]
    } return balik
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// No.6 Metode array
function dataHandling2(input) {
    // Nama
    input.splice(1,1,"Roman Alamsyah Elsharawy")
    console.log(input)
    
    // Tanggal
    var tanggal = input[3]
    var pisahtanggal = tanggal.split("/")
    var bulan = pisahtanggal[1]
    var namabulan = ""
    switch(bulan) {
        case "01":
            namabulan = "Januari"
            break
        case "02":
            namabulan = "Februari"
            break
        case "03":
            namabulan = "Maret"
            break
        case "04":
            namabulan = "April"
            break
        case "05":
            namabulan = "Mei"
            break
        case "06":
            namabulan = "Juni"
            break
        case "07":
            namabulan = "Juli"
            break
        case "08":
            namabulan = "Agustus"
            break
        case "09":
            namabulan = "September"
            break
        case "10":
            namabulan = "Oktober"
            break
        case "11":
            namabulan = "November"
            break
        case "12":
            namabulan = "Desember"
            break
    } console.log(namabulan)

    // Sorting tanggal
    var pisahtanggal2 = pisahtanggal.map(Number)
    var pisahtanggal3 = pisahtanggal2.sort().toString().split(",")
    console.log(pisahtanggal3)
    //date = pisahtanggal.split(",")
    //date.sort(function(value1, value2) { return value1 - value2})
    //console.log(date)

    // Pisah tanggal dengan "-"
    var tanggalBaru = input[3].split("/").join("-")
    console.log(tanggalBaru)

    // Batasi jumlah karakter nama
    var namatoString = String(input[1])
    nama = namatoString.slice(0, 14)
    console.log(nama)
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 