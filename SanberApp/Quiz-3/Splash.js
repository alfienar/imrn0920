import React from 'react'
import { Text, View } from 'react-native'

const Splash = ({
    params,
}) => (
    <View style={styles.home}>
        <Text>Splash</Text>
    </View>
)
const styles = StyleSheet.create({
    home: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default Splash