//No.1 Class
class Score {
    constructor(email, subject, points) {
      this.subject = subject
      this.points = points
      this.email = email
    }
    average() {
      var data = this.points
      var total = 0
      for(var i=0; i < data.length; i++) {
        total += data[i]
      }
      return total / data.length
    }
  }
  
  
  // No.2 Create score
  viewScores = (data, subject) => {
    var n = data.length - 1
    for(i = 1; i < n; i++) {
      var output = {
        email: data[i][0],
        subject: subject,
        points: data[i][data[0].indexOf(subject)]
      }
      console.log(output)
    }
  }
  
  const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
  
  viewScores(data, "quiz-1")
  
  
  // No.3 Recap score
  recapScores = (data) => {
    var nilai = [0]
    var predikat = [""]
    for(var i = 1; i < data.length; i++) {
      rata2 = 0
      for(var j = 1; j < data[i].length; j++) {
        rata2 += data[i][j]
      }
      nilai.push(rata2 / (data[i].length - 1))
      
      if(nilai[i] > 90) {
        predikat.push("honour")
      } else if(nilai[i] > 80 && nilai[i] <= 90) {
        predikat.push("graduate")
      } else if(nilai[i] > 70 && nilai[i] <= 80) {
        predikat.push("participant")
      }
    }
  
    var n = data.length
    for(i = 1; i < n; i++) {
      var output = {
        Email: data[i][0],
        Rata_rata: nilai[i],
        Predikat: predikat[i]
      }
      console.log(output)
    }
  }
  
  const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
  
  recapScores(data)