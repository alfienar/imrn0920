// No.1 Array to object
function arrayToObject(arr=[]) {
    var now = new Date()
    var thisYear = now.getFullYear()
    var n = arr.length
    var bio = {}
    for(i = 0; i < n; i++) {
        for(j = 0; j < 4; j++) {
            switch(j) {
                case 0:
                    bio.firstName = arr[i][0]
                    break
                case 1:
                    bio.lastName = arr[i][1]
                    break
                case 2:
                    bio.gender = arr[i][2]
                    break
                case 3:
                    if(arr[i][3] < thisYear){
                        bio.age = thisYear - arr[i][3]
                    } else {
                        bio.age = "Invalid birth year"
                    }
                    break
            } 
        } 
        console.log(i+1 + ". " + bio.firstName + " " + bio.lastName + ": ")
        console.log(bio)
        //console.log("\n")
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)

// Error case 
arrayToObject([])


// No.2 Shopping time
function shoppingTime(memberId="", money=""){
    var item = ["Sepatu Stacattu", "Baju Zoro", "Baju H&N", "Sweater Uniklooh", "Casing Handphone"]
    var sale = {
        "Sepatu Stacattu": 1500000,
        "Baju Zoro": 500000,
        "Baju H&N": 250000,
        "Sweater Uniklooh": 175000,
        "Casing Handphone": 50000
    }

    var saleItem = []
    var sisaUang = money
    
    if(sisaUang >= 50000){
        while(sisaUang > 0){
            if(sisaUang >= sale["Sepatu Stacattu"]){
                saleItem.push(item[0])
                sisaUang -= sale["Sepatu Stacattu"]
            }
            else if((sisaUang < sale["Sepatu Stacattu"]) && (sisaUang >= sale["Baju Zoro"])){
                saleItem.push(item[1])
                sisaUang -= sale["Baju Zoro"]
            }
            else if((sisaUang < sale["Baju Zoro"]) && (sisaUang >= sale["Baju H&N"])){
                saleItem.push(item[2])
                sisaUang -= sale["Baju H&N"]
            }
            else if((sisaUang < sale["Baju H&N"]) && (sisaUang >= sale["Sweater Uniklooh"])){
                saleItem.push(item[3])
                sisaUang -= sale["Sweater Uniklooh"]
            }
            else{
                saleItem.push(item[4])
                sisaUang -= sale["Casing Handphone"]
                break
            }
        }
    }
    else if((sisaUang < 50000) && (sisaUang > 0)){
        return "Mohon maaf, uang tidak cukup"
    }
    

    // output
    if(memberId === ""){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    else{
        var shopping = {
            memberId: String(memberId),
            money: Number(money),
            listPurchased: [
                saleItem
            ],
            changeMoney: sisaUang
        }
        return shopping
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('', 2475000))
console.log(shoppingTime('234JdhweRxa53', 15000))
console.log(shoppingTime())


// No.3 Naik angkot
function naikAngkot(arrPenumpang=[]){
    rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var n = arrPenumpang.length
    var list = {}

    // index biaya
    var dari = {
        "A": 1,
        "B": 2,
        "C": 3,
        "D": 4,
        "E": 5,
        "F": 6
    }

    var ke = {
        "A": 1,
        "B": 2,
        "C": 3,
        "D": 4,
        "E": 5,
        "F": 6
    }

    for(i = 0; i < n; i++) {
        for(j = 0; j < 4; j++) {
            switch(j){
                case 0:
                    list.penumpang = arrPenumpang[i][0]
                    break
                case 1:
                    list.naikDari = arrPenumpang[i][1]
                    break
                case 2:
                    list.tujuan = arrPenumpang[i][2]
                    break
                case 3:
                    list.bayar = (ke[arrPenumpang[i][2]] - dari[arrPenumpang[i][1]]) * 2000
                    break
            }
        } return list
    }
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]))