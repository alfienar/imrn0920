// No.1 Looping while
console.log('LOOPING PERTAMA');
var num = 0;
while(num<20) {
    num += 2;
    console.log(num + ' - I love coding');
}

console.log('LOOPING KEDUA');
while(num>0) {
    console.log(num + ' - I will become a mobile developer');
    num -= 2;
}

// No.2 Looping for
for(var angka = 1; angka<=20; angka++) {
    if(angka % 2 === 1) {
        console.log(angka +' - Berkualitas');
    } else if((angka % 3 === 0) && (angka % 2 === 1)) {
        console.log(angka + ' - I Love Coding');
    } else {
        console.log(angka + ' - Santai');
    }
}

// No.3 Membuat persegi panjang
for(var obj = 1; obj<=32; obj++) {
    if(obj % 8 !== 0) {
        process.stdout.write('#');
    } else {
        console.log('');
    }
}

// No.4 Membuat tangga
for(var count = 1; count<=7; count++) {
    console.log('#'.repeat(count));
}

// No.5 Membuat papan catur
for(var obj = 0; obj<8; obj++) {
    for(var obj2 = 0; obj2<8; obj2++) {
        if((obj + obj2) % 2 == 0) {
            process.stdout.write(' ');
        } else {
            process.stdout.write('#');
        }
    } console.log('\n');
}